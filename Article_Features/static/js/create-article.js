$(document).ready(function () {
    $(document).on('submit', '#create-submission', function (e){
        e.preventDefault();
        var x = $('#create-submission')
        $.ajax({
            url: '/articles/create-article/',
            type: 'POST',
            data: new FormData(this),
            processData: false,
            contentType: false,
            header: {'X-CSRFtoken' : '{% csrf_token %}'},
            success: function (data) {
                var status = data.boolean
                console.log(status)
                if (status) {
                    alert("Success")
                    window.location.href = '/articles/'
                } else {
                    alert("Failed")
                }
            }
        })
    })
})