$(document).ready(function () {
    var searchWrapper = document.querySelector('.search-input')
    var inputBox = searchWrapper.querySelector('input')
    var suggBox = searchWrapper.querySelector('.autocom-box')

    $(inputBox).keyup(function () {
        var keyword = $(this).val()
        if (keyword !== '') {
            console.log(keyword)
            $.ajax({
                url: '/articles/search/?q=' + keyword,
                type: 'GET',
                success: function (data) {
                    list = []
                    for(i = 0; i < data.items.length; i++) {
                        var tittle = data.items[i].tittle;
                        var id = data.items[i].id;
                        var source = '/articles/'
                        list.push('<li>' + '<a href='+source+id+'>' +
                            tittle + '</a>' + '</li>' )
                    }
                    if (list.length !== 0) {
                        suggBox.innerHTML = list.join('');
                        searchWrapper.classList.add("active")
                    } else {
                        suggBox.innerHTML = '';
                        searchWrapper.classList.remove("active")
                    }
                }
            })
        } else {
            suggBox.innerHTML = '';
            searchWrapper.classList.remove("active")
        }
    })

    $('.con').on('click', '.delete-article', function () {
        var x = confirm("Are you sure to delete this article?");
        var btn = $(this);
        if(x) {
           $(function () {
               $.ajax({
                   url : btn.attr('data-url'),
                   type : 'get',
                   success : function (data) {
                       var boolean = data.boolean
                       if (boolean) {
                           $(".con").html(data.html);
                       }
                   }
               })
           })
        }
    })

    $('.con').on('click', '.edit-article', function () {
        var btn = $(this);
           $(function () {
               $.ajax({
                   url : btn.attr('data-url'),
                   type : 'get',
                    beforeSend: function () {
                       $("#modal-book").modal("show");
                       },
                   success: function (data) {
                       $("#modal-book .modal-content").html(data.html_form);
                        }
               })
           })
    })

    $("#modal-book").on("submit", ".edit-article", function (e) {
        e.preventDefault();
        console.log("test")
        var form = $(this);
        $.ajax({
            url: form.attr("action"),
            data: new FormData(this),
            type: form.attr("method"),
            dataType: 'json',
            processData: false,
            contentType: false,
            header: {'X-CSRFtoken' : '{% csrf_token %}'},
        success: function (data) {
            if (data.boolean) {
                console.log("success")
                $(".con").html(data.html);
                $("#modal-book").modal("hide");
            } else {
                $("#modal-book .modal-content").html(data.html_form);
            }
            }
        });
    });
})