$(document).ready(function () {
    var searchWrapper = document.querySelector('.search-input')
    var inputBox = searchWrapper.querySelector('input')
    var suggBox = searchWrapper.querySelector('.autocom-box')

    $(inputBox).keyup(function () {
        var keyword = $(this).val()
        if (keyword !== '') {
            console.log(keyword)
            $.ajax({
                url: '/articles/search/?q=' + keyword,
                type: 'GET',
                success: function (data) {
                    list = []
                    for(i = 0; i < data.items.length; i++) {
                        var tittle = data.items[i].tittle;
                        var id = data.items[i].id;
                        var source = '/articles/'
                        list.push('<li>' + '<a href='+source+id+'>' +
                            tittle + '</a>' + '</li>' )
                    }
                    if (list.length !== 0) {
                        suggBox.innerHTML = list.join('');
                        searchWrapper.classList.add("active")
                    } else {
                        suggBox.innerHTML = '';
                        searchWrapper.classList.remove("active")
                    }
                }
            })
        } else {
            suggBox.innerHTML = '';
            searchWrapper.classList.remove("active")
        }
    })
})