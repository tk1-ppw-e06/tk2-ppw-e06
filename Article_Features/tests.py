from django.test import TestCase, Client
from django.urls import resolve

from .models import Articles
from .views import listArticles, createArticle, articless, managearticles, deletearticle
from django.core.files.uploadedfile import SimpleUploadedFile, InMemoryUploadedFile
from django.contrib.auth.models import User
from .forms import article

# Create your tests here.

class Test(TestCase):
    def setUp(self):
        self.account = User.objects.create_user(username='admin', email='admin@example.com', password='admin123', first_name='test123', last_name='test123')
        self.account.save()
        return super().setUp()

    def test_model_article(self):
        small_gif = (
            b'\x47\x49\x46\x38\x39\x61\x01\x00\x01\x00\x00\x00\x00\x21\xf9\x04'
            b'\x01\x0a\x00\x01\x00\x2c\x00\x00\x00\x00\x01\x00\x01\x00\x00\x02'
            b'\x02\x4c\x01\x00\x3b'
        )
        image = SimpleUploadedFile('small.gif', small_gif, content_type='image/gif')

        Articles.objects.create(tittle = "AIUEO", author = self.account, description = "PPW is Fun", image = image)
        count = Articles.objects.all().count()
        self.assertEqual(count, 1)
        test = Articles.objects.get(tittle="AIUEO")
        self.assertEqual(str(test), "AIUEO")
        response = Client().get('/articles/1/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'Article_Features/article.html')
        found = resolve('/articles/1/')
        self.assertEqual(found.func, articless)


    def test_url_list_articles(self):
        response = Client().get('/articles/')
        self.assertEqual(response.status_code, 200)

    def test_template_list_articles(self):
        response = Client().get('/articles/')
        self.assertTemplateUsed(response, 'Article_Features/list-articles.html')

    def test_index_func_list_articles(self):
        found = resolve('/articles/')
        self.assertEqual(found.func, listArticles)

    def test_url_create_article_Not_Login(self):
        response = Client().get('/articles/create-article/')
        self.assertEqual(response.status_code, 302)

    def test_url_create_article_Login(self):
        c = Client()
        c.login(username='admin', password='admin123')
        response = c.get('/articles/create-article/')
        self.assertEqual(response.status_code, 200)

    def test_template_create_article(self):
        c = Client()
        c.login(username='admin', password='admin123')
        response = c.get('/articles/create-article/')
        self.assertTemplateUsed(response, 'Article_Features/create-article.html')

    def test_index_func_create_article(self):
        found = resolve('/articles/create-article/')
        self.assertEqual(found.func, createArticle)

    def test_form_crate_article_success(self):
        c = Client()
        c.login(username='admin', password='admin123')
        small_gif = (
            b'\x47\x49\x46\x38\x39\x61\x01\x00\x01\x00\x00\x00\x00\x21\xf9\x04'
            b'\x01\x0a\x00\x01\x00\x2c\x00\x00\x00\x00\x01\x00\x01\x00\x00\x02'
            b'\x02\x4c\x01\x00\x3b'
        )
        image = SimpleUploadedFile('small.gif', small_gif, content_type='image/gif')
        response = c.post('/articles/create-article/', data={'tittle': 'AIUEO', 'author': self.account.id, 'description':'test123', 'image' : image})
        self.assertEqual(response.status_code, 200)
        isi = response.content.decode('utf8')
        self.assertIn('true', isi)

    def test_form_crate_article_failed(self):
        c = Client()
        c.login(username='admin', password='admin123')
        small_gif = (
            b'\x47\x49\x46\x38\x39\x61\x01\x00\x01\x00\x00\x00\x00\x21\xf9\x04'
            b'\x01\x0a\x00\x01\x00\x2c\x00\x00\x00\x00\x01\x00\x01\x00\x00\x02'
            b'\x02\x4c\x01\x00\x3b'
        )
        image = SimpleUploadedFile('small.gif', small_gif, content_type='image/gif')
        response = c.post('/articles/create-article/', data={'tittle': 'AIUEO', 'description':'test123', 'image' : image})
        self.assertEqual(response.status_code, 200)
        isi = response.content.decode('utf8')
        self.assertIn('false', isi)


    def test_view_list_article(self):
        response = Client().get('/articles/')
        isi_html_kembalian = response.content.decode('utf8')
        self.assertIn('Health Articles', isi_html_kembalian)

    def test_view_create_article(self):
        c = Client()
        c.login(username='admin', password='admin123')
        response = c.get('/articles/create-article/')
        isi_html_kembalian = response.content.decode('utf8')
        self.assertIn('Create Article', isi_html_kembalian)
        self.assertIn('Tittle', isi_html_kembalian)
        self.assertIn('Author', isi_html_kembalian)
        self.assertIn('Description', isi_html_kembalian)

    def test_form_article_is_invalid(self):
        data = {}
        form = article(data)
        self.assertFalse(form.is_valid())

    def test_ajax_search_article_is_exist(self):
        c = Client()
        c.login(username='admin', password='admin123')
        small_gif = (
            b'\x47\x49\x46\x38\x39\x61\x01\x00\x01\x00\x00\x00\x00\x21\xf9\x04'
            b'\x01\x0a\x00\x01\x00\x2c\x00\x00\x00\x00\x01\x00\x01\x00\x00\x02'
            b'\x02\x4c\x01\x00\x3b'
        )
        image = SimpleUploadedFile('small.gif', small_gif, content_type='image/gif')
        c.post('/articles/create-article/', data={'tittle': 'AIUEO', 'author': self.account.id, 'description': 'test123', 'image': image})
        response = c.get('/articles/search/?q=AIUEO')
        isi = response.content.decode('utf8')
        self.assertIn('AIUEO', isi)
        self.assertIn('test123', isi)
        self.assertEqual(response.status_code, 200)

    def test_ajax_search_article_is_not_exist(self):
        c = Client()
        c.login(username='admin', password='admin123')
        small_gif = (
            b'\x47\x49\x46\x38\x39\x61\x01\x00\x01\x00\x00\x00\x00\x21\xf9\x04'
            b'\x01\x0a\x00\x01\x00\x2c\x00\x00\x00\x00\x01\x00\x01\x00\x00\x02'
            b'\x02\x4c\x01\x00\x3b'
        )
        image = SimpleUploadedFile('small.gif', small_gif, content_type='image/gif')
        c.post('/articles/create-article/', data={'tittle': 'EOUIA', 'author': self.account.id, 'description': 'test123', 'image': image})
        response = c.get('/articles/search/?q=AIUEO')
        isi = response.content.decode('utf8')
        self.assertNotIn('AIUEO', isi)
        self.assertNotIn('Asep', isi)
        self.assertEqual(response.status_code, 200)

    def test_url_manage_articles(self):
        c = Client()
        c.login(username='admin', password='admin123')
        response = c.get('/articles/manage/')
        self.assertEqual(response.status_code, 200)

    def test_template_manage_articles(self):
        c = Client()
        c.login(username='admin', password='admin123')
        response = c.get('/articles/manage/')
        self.assertTemplateUsed(response, 'Article_Features/manage-articles.html')

    def test_index_func_manage_articles(self):
        c = Client()
        c.login(username='admin', password='admin123')
        found = resolve('/articles/manage/')
        self.assertEqual(found.func, managearticles)

    def test_url_manage_article_Not_Login(self):
        response = Client().get('/articles/manage/')
        self.assertEqual(response.status_code, 302)

    def test_url_delete_articles(self):
        c = Client()
        c.login(username='admin', password='admin123')
        small_gif = (
            b'\x47\x49\x46\x38\x39\x61\x01\x00\x01\x00\x00\x00\x00\x21\xf9\x04'
            b'\x01\x0a\x00\x01\x00\x2c\x00\x00\x00\x00\x01\x00\x01\x00\x00\x02'
            b'\x02\x4c\x01\x00\x3b'
        )
        image = SimpleUploadedFile('small.gif', small_gif, content_type='image/gif')
        c.post('/articles/create-article/',data={'tittle': 'EOUIA', 'author': self.account.id, 'description': 'test123', 'image': image})
        response = c.get('/articles/delete/1/')
        self.assertEqual(response.status_code, 200)

    def test_url_delete_article_Not_Login(self):
        response = Client().get('/articles/delete/1/')
        self.assertEqual(response.status_code, 302)


    def test_url_get_edit_articles(self):
        c = Client()
        c.login(username='admin', password='admin123')
        small_gif = (
            b'\x47\x49\x46\x38\x39\x61\x01\x00\x01\x00\x00\x00\x00\x21\xf9\x04'
            b'\x01\x0a\x00\x01\x00\x2c\x00\x00\x00\x00\x01\x00\x01\x00\x00\x02'
            b'\x02\x4c\x01\x00\x3b'
        )
        image = SimpleUploadedFile('small.gif', small_gif, content_type='image/gif')
        c.post('/articles/create-article/',data={'tittle': 'EOUIA', 'author': self.account.id, 'description': 'test123', 'image': image})
        response = c.get('/articles/edit/1/')
        self.assertEqual(response.status_code, 200)

    def test_url_edit_article_Not_Login(self):
        response = Client().get('/articles/edit/1/')
        self.assertEqual(response.status_code, 302)

    def test_url_post_edit_articles(self):
        c = Client()
        c.login(username='admin', password='admin123')
        small_gif = (
            b'\x47\x49\x46\x38\x39\x61\x01\x00\x01\x00\x00\x00\x00\x21\xf9\x04'
            b'\x01\x0a\x00\x01\x00\x2c\x00\x00\x00\x00\x01\x00\x01\x00\x00\x02'
            b'\x02\x4c\x01\x00\x3b'
        )
        image = SimpleUploadedFile('small.gif', small_gif, content_type='image/gif')
        c.post('/articles/create-article/',data={'tittle': 'EOUIA', 'author': self.account.id, 'description': 'test123', 'image': image})
        response = c.post('/articles/edit/1/', data={'tittle': 'AIUEO', 'author': self.account.id, 'description': 'test123', 'image': image})
        self.assertEqual(response.status_code, 200)