from django.urls import path

from . import views

app_name = 'articles'

urlpatterns = [
    path('', views.listArticles, name='list-articles'),
    path('create-article/', views.createArticle, name='create-article'),
    path('<int:id>/', views.articless, name='article'),
    path('search/', views.searcharticle, name='search'),
    path('manage/', views.managearticles, name='manage-articles'),
    path('delete/<int:id>/', views.deletearticle, name='delete-article'),
    path('edit/<int:id>/', views.editarticle, name='edit-article')
]