from django.apps import AppConfig


class ArticleFeaturesConfig(AppConfig):
    name = 'Article_Features'
