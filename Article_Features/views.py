from django.forms import model_to_dict
from django.shortcuts import render, redirect

# Create your views here.

from django import forms
from django.http import HttpResponse, JsonResponse
from cloudinary.forms import cl_init_js_callbacks
from django.template.loader import render_to_string

from .models import Articles
from .forms import article
from django.contrib.auth.models import User

def createArticle(request):
    if request.user.is_authenticated:
        user = User.objects.get(username=request.user.username)
        test = article(initial={'author' : user.id})
        context = { 'backend_form' : test,}
        if request.method == 'POST':
            form = article(request.POST, request.FILES)
            context['posted'] = form.instance
            if form.is_valid():
                form.save()
                data = {'boolean' : True}
                return JsonResponse(data, safe=False)
            else:
                data = {'boolean' : False}
                return JsonResponse(data, safe=False)
        else:
            return render(request, 'Article_Features/create-article.html', context)
    else:
        return redirect('articles:list-articles')

def listArticles(request):
    articles = Articles.objects.all()
    context = {'articles' : articles,
               'type' : 'list'}
    return render(request, 'Article_Features/list-articles.html', context)

def articless(request, id):
    arc = Articles.objects.get(id=id)
    context = {'article' : arc}
    return render(request, 'Article_Features/article.html', context)

def searcharticle(request):
    arg = request.GET['q']
    data = {'size' : 0, 'items' : []}
    datas = Articles.objects.filter(tittle__istartswith=arg)
    data['size'] = Articles.objects.filter(tittle__istartswith=arg).count()
    item = data['items']
    for d in datas:

        y = {'tittle' : d.tittle,
               'author' : d.author.get_full_name(),
               'image' : d.image.url,
                'id' : d.id,
              }
        item.append(y)
    return JsonResponse(data, safe=False)

def managearticles(request):
    if request.user.is_authenticated:
        articles = Articles.objects.filter(author=request.user)
        context = {'articles': articles,
                   'type' : 'manage'}
        return render(request, 'Article_Features/manage-articles.html', context)
    else:
        return redirect('articles:list-articles')

def deletearticle(request, id):
    if request.user.is_authenticated:
        Article = Articles.objects.get(id=id)
        data = {}
        if request.method == 'GET':
            Article.delete()
            data['boolean'] = True
            articles = Articles.objects.filter(author=request.user)
            context = {'articles': articles}
            data['html'] = render_to_string('Article_Features/card_articles.html', context, request)
        return JsonResponse(data, safe=False)
    else:
        return redirect('articles:manage-articles')

def editarticle(request, id):
    if request.user.is_authenticated:
        Article = Articles.objects.get(id=id)
        data = dict()
        if request.method == 'POST':
            form = article(request.POST, request.FILES, instance=Article)
            if form.is_valid():
                form.save()
                data['boolean'] = True
                articles = Articles.objects.filter(author=request.user)
                arc = {'articles': articles}
                data['html'] = render_to_string('Article_Features/card_articles.html', arc, request)
        else:
            form = article(instance=Article)
        context = {'backend_form' : form}
        data['html_form'] = render_to_string('Article_Features/modal-edit-article.html', context, request=request)
        return JsonResponse(data, safe=False)
    else:
        return redirect('articles:manage-articles')