# Tugas Kelompok 2 (E06)

[![pipeline status](https://gitlab.com/tk1-ppw-e06/tk2-ppw-e06/badges/master/pipeline.svg)](https://gitlab.com/tk1-ppw-e06/tk2-ppw-e06/-/commits/master)
[![coverage report](https://gitlab.com/tk1-ppw-e06/tk2-ppw-e06/badges/master/coverage.svg)](https://gitlab.com/tk1-ppw-e06/tk2-ppw-e06/-/commits/master)

Repositori ini berisi sebuah templat untuk membuat proyek Django yang siap
di-*deploy* ke Heroku melalui GitHub Actions atau GitLab CI.


# Nama-nama anggota

- Muhammad Farhan Ramadhan     - 1906306155
- Fiorentine Phillips          - 1906298916 
- Faizah                       - 1906298885
- Rizma Chaerani Heidy Putri   - 1906299111
- Mochammad Naufal Rizki       - 1906299010

# Link Herokuapp 

https://healthlab.herokuapp.com/

# Deskripsi TK
Aplikasi yang kelompok kami buat adalah HealthLab. HealthLab adalah aplikasi penjaga kesehatan dan kebugaran yang membantu orang-orang pada kegiatan sehari-harinya. Pada masa pandemi Covid-19 ini, yang mengharuskan masyarakat untuk tetap didalam rumah dapat menyebabkan kurangnya aktivitas fisik dan makan makanan yang sehat untuk menjaga imunitas tubuh, oleh sebab itu HealthLab akan membantu menjadi sumber informasi dan sarana untuk masyarakat menjaga kesehatannya.

Di HealthLab ada beberapa sarana untuk membantu orang-orang dalam mengukur tingkat kesehatannya, seperti BMR(Basal Metabolic Rate) calculator, BMI(Body Mass Index)calculator, dan Calories calculator. Lalu dengan adanya artikel-artikel dan To-do List, dapat menjadi suatu sarana informasi terhadap bagaimana cara hidup sehat dan membantu menerapkan kehidupan sehat tersebut.



# Fitur-fitur

- **BMR** 
    User dapat menghitung estimasi berapa besar BMR (Basal Metabolic Rate).
- **BMI**
    User dapat menghitung estimasi berapa besar BMI (Body Mass Index).
- **Calories**
    User dapat menghitung estimasi berapa besar Calories/week yang dibutuhkan.
- **Article** 
    User dapat melihat berbagai artikel-artikel tentang kesehatan dan dapat membuat artikel kesehatan yang dapat bermanfaat.
- **To-Do List**
    User dapat membuat list-list bagaimana cara untuk menerapkan kehidupan yang sehat pada sehari-hari.

# Pembagian Tugas

- **BMR** Fitur ini dikerjakan oleh Fiorentine Phillips
- **BMI** Fitur ini dikerjakan oleh Rizma Chaerani Haidy Putri
- **Calories** Fitur ini dikerjakan oleh Farhan Ramadhan
- **Article** Fitur ini dikerjakan oleh Naufal Rizki
- **To-Do List** Fitur ini dikerjakan oleh Faizah

