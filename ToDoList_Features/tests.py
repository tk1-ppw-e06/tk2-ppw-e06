from django.test import TestCase, Client
from django.urls import resolve

from django.contrib.auth.models import User
from .models import ToDoList
from .views import TaskList, TaskComplete, TaskDelete

# Create your tests here.
class TestToDoList(TestCase):
    def setUp(self):
        self.account = User.objects.create_user(username='kakpewe', email='kakpewe@example.com', password='test12345')
        self.account.save()
        return super().setUp()

    # Test untuk fitur to-do-list dan add task
    def test_url_task_list_not_login(self):
        c = Client()
        response = c.get('/to-do-list/')  
        self.assertEquals(302, response.status_code)

    def test_url_task_list_login(self):
        c = Client()
        c.login(username="kakpewe", password="test12345")
        response = c.get('/to-do-list/')  
        self.assertEquals(200, response.status_code)

    def test_template_task_list(self):
        c = Client()
        c.login(username="kakpewe", password="test12345")
        response = c.get('/to-do-list/')  
        self.assertTemplateUsed(response, 'ToDoList_Features/task_list.html')

    def test_model_to_do_list(self):
        ToDoList.objects.create(todo = "Do TK2 PPW")
        count = ToDoList.objects.all().count()
        self.assertEqual(count, 1)
        test = ToDoList.objects.get(todo = "Do TK2 PPW")
        self.assertEqual(str(test), "Do TK2 PPW")

    # Test untuk fitur completed task
    def test_url_task_completed_JsonResponse_not_login(self):
        c = Client()
        ToDoList.objects.create(todo = "Do TK2 PPW")
        todos_count = ToDoList.objects.all().count()
        if (todos_count > 0):
            JsonResponse = c.get('/to-do-list/1/completed/')
            self.assertEquals(302, JsonResponse.status_code)

    def test_url_task_completed_JsonResponse_login(self):
        c = Client()
        c.login(username="kakpewe", password="test12345")

        ToDoList.objects.create(todo = "Do TK2 PPW")
        todos_count = ToDoList.objects.all().count()
        if (todos_count > 0):
            JsonResponse = c.get('/to-do-list/1/completed/')
            self.assertEquals(200, JsonResponse.status_code)

    def test_view_func_task_completed(self):
        found = resolve('/to-do-list/<str:id>/completed/')
        self.assertEqual(found.func, TaskComplete)

    # Test untuk fitur delete task
    def test_url_task_delete_JsonResponse_not_login(self):
        c = Client()
        ToDoList.objects.create(todo = "Do TK2 PPW")
        todos_count = ToDoList.objects.all().count()
        if (todos_count > 0):
            JsonResponse = c.get('/to-do-list/1/delete/')
            self.assertEquals(302, JsonResponse.status_code)

    def test_url_task_delete_JsonResponse_login(self):
        c = Client()
        c.login(username="kakpewe", password="test12345")

        ToDoList.objects.create(todo = "Do TK2 PPW")
        todos_count = ToDoList.objects.all().count()
        if (todos_count > 0):
            JsonResponse = c.get('/to-do-list/1/delete/')
            self.assertEquals(200, JsonResponse.status_code)

    def test_view_func_task_delete(self):
        found = resolve('/to-do-list/<str:id>/delete/')
        self.assertEqual(found.func, TaskDelete)
