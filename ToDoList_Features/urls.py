from django.urls import path

from . import views
from .views import TaskList

app_name = 'to-do-list'

urlpatterns = [
    path('', TaskList.as_view(), name='task_list'),
    path('<str:id>/completed/', views.TaskComplete, name='task_list_complete'),
    path('<str:id>/delete/', views.TaskDelete, name='task_list_delete'),
]
