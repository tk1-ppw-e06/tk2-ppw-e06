from django.shortcuts import render, redirect
from django.views.generic import View
from django import forms
from django.http import HttpResponse, JsonResponse
from django.forms.models import model_to_dict

from .models import ToDoList
from .forms import ToDoForm

# Create your views here.
class TaskList(View):
    
    def get(self, request):
        if request.user.is_authenticated:
            form = ToDoForm()
            tasks = ToDoList.objects.all()
            return render(request, 'ToDoList_Features/task_list.html', context={'form': form, 'tasks': tasks})
        else:
            return redirect('user:login')

    def post(self, request):
        if request.user.is_authenticated:
            form = ToDoForm(request.POST)

            if form.is_valid():
                new_task = form.save()
                return JsonResponse({'task': model_to_dict(new_task)}, status=200)
            else:
                return redirect('to-do-list:task_list')
        else:
            return redirect('user:login')

def TaskComplete(request, id):
    if request.user.is_authenticated:
        task = ToDoList.objects.get(id=id)
        task.completed = True
        task.save()
        return JsonResponse({'task': model_to_dict(task)}, status=200)
    else:
        return redirect('user:login')

def TaskDelete(request, id):
    if request.user.is_authenticated:
        task = ToDoList.objects.get(id=id)
        task.delete()
        return JsonResponse({'result': 'ok'}, status=200)
    else:
        return redirect('user:login')
