$(document).ready(function(){

    var csrfToken = $("input[name=csrfmiddlewaretoken]").val();

    $("#createButton").click(function(){
        var serializedData = $("#createTaskForm").serialize();
        
        $.ajax({
            url: $("#createTaskForm").data('url'),
            data: serializedData,
            type: 'post',
            success: function(response) {
                $("#taskList").append('<div class="item-row" id="task-row" data-id="' + response.task.id + '" style="color: black; text-align: justify;">' +
                    response.task.todo + '<button type="button" class="close float-right" data-id="' + response.task.id + '" >' +
                    '<span aria-hidden="true">&times;</span></button></div>')
            }
        })
        $("#createTaskForm")[0].reset();
    });

    $("#createTaskForm").keypress(function(e){
        var serializedData = $("#createTaskForm").serialize();
        var keycode = (e.keyCode ? e.keyCode : e.which);
        if(keycode == '13'){
            $.ajax({
                url: $("#createTaskForm").data('url'),
                data: serializedData,
                type: 'post',
                success: function(response) {
                    $("#taskList").append('<div class="item-row" id="task-row" data-id="' + response.task.id + '" style="color: black; text-align: justify;">' +
                        response.task.todo + '<button type="button" class="close float-right" data-id="' + response.task.id + '" >' +
                        '<span aria-hidden="true">&times;</span></button></div>')
                }
            })
            $("#createTaskForm")[0].reset();
        }
    });

    // $('#someTextBox').keypress(function(event){
    //     var keycode = (event.keyCode ? event.keyCode : event.which);
    //     if(keycode == '13'){
    //         alert('You pressed a "enter" key in textbox');  
    //     }
    // });

    $("#taskList").on('click', '.item-row', function(){
        var dataId = $(this).data('id');

        $.ajax({
            url: '/to-do-list/' + dataId + '/completed/',
            data: {
                csrfmiddlewaretoken: csrfToken,
                id: dataId
            },
            type: 'post',
            success: function() {
                var taskItem = $('#task-row[data-id="' + dataId + '"]');
                taskItem.css('text-decoration', 'line-through').hide().slideDown();
                $("#taskList").append(taskItem);
            }
        });
    }).on('click', 'button.close', function(event){
        event.stopPropagation();

        var dataId = $(this).data('id');

        $.ajax({
            url: '/to-do-list/' + dataId + '/delete/',
            data: {
                csrfmiddlewaretoken: csrfToken,
                id: dataId
            },
            type: 'post',
            success: function() {
                $('#task-row[data-id="' + dataId + '"]').remove();
            }
        })
    });
});
