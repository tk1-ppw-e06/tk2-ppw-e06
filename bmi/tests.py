from django.test import TestCase, Client
from django.urls import resolve
from .models import BMI 
from . import views
from Article_Features.models import Articles
from django.core.files.uploadedfile import SimpleUploadedFile
from django.contrib.auth.models import User
from .forms import FormBMI

# Create your tests here.
class BMIUnitTest(TestCase):
    def setUp(self):
        self.account = User.objects.create_user(username='admin', email='admin@example.com', password='admin123',
                                                first_name='test123', last_name='test123')
        self.account.save()
        return super().setUp()

    def test_url_bmi_exist(self):
        response = Client().get('/bmi/')
        self.assertEqual(response.status_code, 200)

    def test_template_bmi_used(self):
        response = Client().get('/bmi/')
        self.assertTemplateUsed(response, 'BMIhome.html')

    def test_model_can_create_new_data(self):
        data = BMI.objects.create(height='1', weight='1')
        test_data = BMI.objects.all().count()
        self.assertEqual(test_data, 1)
    
    def test_app_title(self):
        response = Client().get('/bmi/')
        page_Content = response.content.decode('utf8')
        self.assertIn("BMI Calculator", page_Content)
    
    # def test_calculate_button_exist(self):
    #     response = Client().get('/bmi/')
    #     page_content = response.content.decode('utf8')
    #     self.assertIn('<button style="background-image: linear-gradient(#EC4057, #F57434);" type="submit" class="btn btn-danger mb-3">SIGN UP</button>', page_content)

    def test_view_forms(self):
        found = resolve('/bmi/')
        self.assertEqual(found.func, views.BMIHome)

    def test_url_post_BMI_result_is_valid_not_login(self):
        response = Client().post('/bmi/', {'height' : 1, 'weight' : 1})
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'BMIResult.html')

    def test_url_post_BMI_result_is_valid_has_login(self):
        c = Client()
        c.login(username='admin', password='admin123')
        small_gif = (
            b'\x47\x49\x46\x38\x39\x61\x01\x00\x01\x00\x00\x00\x00\x21\xf9\x04'
            b'\x01\x0a\x00\x01\x00\x2c\x00\x00\x00\x00\x01\x00\x01\x00\x00\x02'
            b'\x02\x4c\x01\x00\x3b'
        )
        image = SimpleUploadedFile('small.gif', small_gif, content_type='image/gif')
        Articles.objects.create(
            tittle="Body Mass Index and Body Fat",
            author=self.account, description="PPW is Fun", image=image)
        response = c.post('/bmi/', {'height': 1, 'weight': 1})
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'BMIResult.html')

    def test_form_article_invalid(self):
        data = {}
        form = FormBMI(data)
        self.assertFalse(form.is_valid())

    def test_form_article_is_valid(self):
        data = {'height' : 1, 'weight' : 1}
        form = FormBMI(data)
        self.assertTrue(form.is_valid())