$(document).ready(function(){
    $('#calform').hide().slideDown(1000);

    $('#calarticle').hide().show(1000);

    $('#res').hide().slideDown(1000);

    $("#title").click(function(){
        $("#calform").toggle();
    });
});

$(document).ready(function () {
    $('.test').click(function () {
        var height = $('.height').val()
        var weight = $('.weight').val()
        if(height !== '' & weight !== '') {
            $.ajax({
                url: '/bmi/submit/',
                type: 'GET',
                data: {'height' : height, 'weight' : weight},
                success: function (data) {
                    var result = data.result
                    if (result !== 'invalid') {
                        alert("Your result: " + result)
                    } else {
                        alert("Error result")
                    }
                }
            })
        } else {
            alert("Error result")
        }
    })
});