from django.shortcuts import render, redirect
from .forms import FormBMI
from .models import BMI
from Article_Features.models import Articles
from django.http import HttpResponse, JsonResponse

# Create your views here.
def BMIHome(request):
    form = FormBMI()
    if request.method == 'POST':
        form = FormBMI(request.POST)
        response_data = {}
        response_data['height'] = request.POST['height']
        response_data['weight'] = request.POST['weight']
        result = int(response_data['weight']) / ((int(response_data['height'])/100)**2)
        # data = BMI(height=response_data['height'], weight=response_data['weight'])
        if form.is_valid():
            form.save()
            if request.user.is_authenticated:
                articles = Articles.objects.get(tittle='Body Mass Index and Body Fat')
                return render(request, 'BMIResult.html', {'result' : result, 'form' : form, 'article' : articles})
            else:
                return render(request, 'BMIResult.html', {'result': result, 'form': form})


    return render(request, 'BMIhome.html', {'form' : form})

def BMIResult(request):
    return render(request, 'BMIResult.html')

def submit(request):
    data = dict()
    weight = request.GET['weight']
    height = request.GET['height']
    try :
        data['result'] = int(weight) / ((int(height)/100)**2)
    except:
        data['result'] = 'invalid'
    
    return JsonResponse(data, safe=False)