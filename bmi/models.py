from django.db import models
from django import forms

# Create your models here.
class BMI(models.Model):
    height = models.CharField(blank=False, default='1', max_length=10)
    weight = models.CharField(blank=False, default='1', max_length=10)