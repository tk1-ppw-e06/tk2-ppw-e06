from django import forms
from .models import BMI

class FormBMI(forms.ModelForm):
    class Meta:
        model = BMI
        fields = ('height', 'weight')

        widgets = {
            'height' : forms.TextInput(attrs={'class' : 'form-group height', 'style' : 'width: 400px; border-radius: 10px'}),
            'weight' : forms.TextInput(attrs={'class' : 'form-group weight', 'style' : 'width: 400px; border-radius: 10px'}),
        }