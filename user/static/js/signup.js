$(document).ready(function () {
    var valusername;
    var valpassword1;
    var valpassword2;
    $(".form-group").on('keyup', '#username', function () {
        var username = $(this).val();
        valusername = username;
        //console.log(username)
        $.ajax({
            url: '/searchusername/?q=' + username,
            type: 'GET',
            success: function (data) {
                var message = $('.messageusername');
                message.empty();
                var bool = data.boolean;
                if (bool === "true") {
                    message.append('<small>Username already exist</small>')
                } else if (bool === "false" && data.username !== '')
                    message.append('<small>Username can use</small>')
            }
        })
    })

    $("#password1").keyup(function () {
        var password1 = $(this).val();
        valpassword1 = password1;
        var message = $(".messagepasssword1");
        message.empty();
        if (password1.length !== 0 &&password1.length < 8) {
            message.append('<small>This password is too short. It must contain at least 8 characters.</small>')
        }
    })

    $("#password2").keyup(function () {
        var password2 = $(this).val();
        valpassword2 = password2;
        var message = $(".messagepasssword2");
        message.empty();
        if (password1.length !== 0 &&password2.length < 8) {
            message.append('<small>This password is too short. It must contain at least 8 characters.</small>')
        } else if (valpassword1 !== password2) {
            message.append('<small>password not match</small>')
        }
    })

    $(document).on('submit', '#signup-submission', function (e){
        e.preventDefault();
        var x = $('#signup-submission');
        $.ajax({
            url: '/user/signup/',
            type: 'POST',
            data: x.serialize(),
            header: {'X-CSRFtoken' : '{% csrf_token %}'},
            success: function (data) {
                var status = data.success;
                if (status) {
                    alert("Success")
                    window.location.href = '/'
                } else {
                    alert("Username or Password invalid")
                }
            }
        })
    })
})