$(document).ready(function () {
    $(document).on('submit', '#login-submission', function (e){
        e.preventDefault();
        var x = $('#login-submission');
        $.ajax({
            url: '/user/login/',
            type: 'POST',
            data: x.serialize(),
            header: {'X-CSRFtoken' : '{% csrf_token %}'},
            success: function (data) {
                var status = data.success
                if (status) {
                    alert("Success")
                    window.location.href = '/'
                } else {
                    alert("Username or Password invalid")
                }
            }
        })
    })
})