from django.test import TestCase, Client
from django.urls import resolve
from django.contrib.auth.models import User
from django.contrib.auth import authenticate
from .views import LogIn, SignUp, LogOut

# Create your tests here.

class Test(TestCase):
    def setUp(self):
        self.user = {
            'email' : 'test@example.com',
            'username' : 'test',
            'password1' : 'testtest123',
            'password2' : 'testtest123',
            'first_name' : 'Ujang',
            'last_name' : 'Markujang',
        }
        self.user_sigunup_failed = {
            'email': 'test@example.com',
            'username': 'test',
            'password1': 'testtest123',
            'password2': 'testtest1234',
        }

        self.account = User.objects.create_user(username='admin', email='admin@example.com', password='admin123',
                                                    first_name='test123', last_name='test123')
        self.account.save()
        return super().setUp()

    def test_url_LogIn_if_not_login(self):
        response = Client().get('/user/login/')
        self.assertEqual(response.status_code, 200)

    def test_template_LogIn(self):
        response = Client().get('/user/login/')
        self.assertTemplateUsed(response, 'user/login.html')

    def test_index_func_LogIn(self):
        found = resolve('/user/login/')
        self.assertEqual(found.func, LogIn)

    def test_url_SignUp_if_not_login(self):
        response = Client().get('/user/signup/')
        self.assertEqual(response.status_code, 200)

    def test_template_SignUp(self):
        response = Client().get('/user/signup/')
        self.assertTemplateUsed(response, 'user/signup.html')

    def test_index_func_SignUp(self):
        found = resolve('/user/signup/')
        self.assertEqual(found.func, SignUp)

    def test_url_LogOut_if_has_login(self):
        c = Client()
        c.login(username='admin', password='admin123')
        response = c.get('/user/logout/')
        self.assertEqual(response.status_code, 200)

    def test_template_LogOut(self):
        c = Client()
        c.login(username='admin', password='admin123')
        response = c.get('/user/logout/')
        self.assertTemplateUsed(response, 'user/logout.html')

    def test_index_func_LogOut(self):
        found = resolve('/user/logout/')
        self.assertEqual(found.func, LogOut)

    def test_LogIn_Succes(self):
        response_post = Client().post('/user/login/', {'username': 'admin', 'password' : 'admin123'}, format='text/html')
        self.assertEqual(response_post.status_code, 200)
        isi = response_post.content.decode('utf8')
        self.assertIn("true", isi)

    def test_LogIn_Failed(self):
        response_post = Client().post('/user/login/', {'username': 'admin', 'password' : 'admin1'}, format='text/html')
        self.assertEqual(response_post.status_code, 200)
        isi = response_post.content.decode('utf8')
        self.assertIn("false", isi)

    def test_SignUp_Success(self):
        c = Client()
        response = c.post('/user/signup/', self.user, format='text/html')
        self.assertEqual(response.status_code, 200)
        isi = response.content.decode('utf8')
        self.assertIn("true", isi)

    def test_SignUp_Failed(self):
        c = Client()
        response = c.post('/user/signup/', self.user_sigunup_failed, format='text/html')
        self.assertEqual(response.status_code, 200)
        isi = response.content.decode('utf8')
        self.assertIn("false", isi)

    def test_LogOut(self):
        c = Client()
        c.post('/user/signup/', self.user, format='text/html')
        response = c.post('/user/logout/', {'boolean' : 'true'})
        self.assertEqual(response.status_code, 302)

    def test_url_LogIn_if_has_login(self):
       c = Client()
       c.login(username='admin', password='admin123')
       request = c.get('/user/login/')
       self.assertEqual(request.status_code, 302)

    def test_url_SignUp_if_has_login(self):
       c = Client()
       c.login(username='admin', password='admin123')
       request = c.get('/user/signup/')
       self.assertEqual(request.status_code, 302)

    def test_url_LogOut_if_not_login(self):
       request = Client().get('/user/logout/')
       self.assertEqual(request.status_code, 302)