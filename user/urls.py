from django.urls import path

from . import views

app_name = 'user'

urlpatterns = [
    path('login/', views.LogIn, name='login'),
    path('logout/', views.LogOut, name='logout'),
    path('signup/', views.SignUp, name='signup'),
]