import json

from django.contrib.auth import login, logout, authenticate
from django.shortcuts import render, redirect
from .forms import CreateUserForm
from django.http import JsonResponse

# Create your views here.
def SignUp(request):
    if request.user.is_authenticated:
        return redirect('main:home')
    form = CreateUserForm()
    if request.method == 'POST':
        form = CreateUserForm(request.POST)
        data = {}
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password1')
            user = authenticate(request, username=username, password=password)
            login(request, user)
            data['success'] = True
            return JsonResponse(data, safe=False)
        else:
            data['success'] = False
            return JsonResponse(data, safe=False)
    else:
        context = {'form' : form}
        return render(request, 'user/signup.html', context)

def LogOut(request):
    if request.user.is_authenticated:
        if request.method == 'POST':
            boolean = request.POST['boolean']
            if boolean == "true":
                logout(request)
            return redirect('main:home')
        return render(request, 'user/logout.html')
    else:
        return redirect('main:home')

def LogIn(request):
    if request.user.is_authenticated:
        return redirect('main:home')
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        data = {}
        if user is not None:
            data['success'] = True
            login(request, user)
            return JsonResponse(data, safe=False)
        else:
            data['success'] = False
            return JsonResponse(data, safe=False)
    else:
        return render(request, 'user/login.html')

