from django.test import TestCase, Client
from django.urls import resolve

from .models import Calories
from .views import form_cal, inputCal
from .forms import calories

class test_calories_calculator(TestCase):
    def test_response_page(self):
        response = Client().get('/calories-cal/')
        self.assertEqual(response.status_code, 200)

    def test_title_page(self):
        response = Client().get('/calories-cal/')
        title_body_html = response.content.decode('utf8')
        self.assertIn("Calories Calculator", title_body_html)

    def test_views_forms(self):
        found = resolve('/calories-cal/')
        self.assertEqual(found.func, form_cal)
    
    def test_calculate_button(self):
        response = Client().get('/calories-cal/')
        body_button = response.content.decode('utf8')
        self.assertIn('<button type="submit" style="background-image: linear-gradient(#EC4057, #F57434); color: #ffffff; border-radius: 14px;" class="btn btn" id="bton">Calculate</button>', body_button)

    def test_views_res(self):
        found = resolve('/calories-cal/res/')
        self.assertEqual(found.func, inputCal)

    def test_model_calories(self):
        Calories.objects.create(age=1, gender='test', weight=1, height=1, activity='test')
        count = Calories.objects.all().count()
        self.assertEqual(count, 1)

    def test_form_Calories_is_valid(self):
        form = {'age' : 1, 'gender' : 'test', 'weight' : 1, 'height' : 1, 'activity' : 'test'}
        test = calories(form)
        self.assertTrue(test.is_valid())

    def test_form_Calories_is_notvalid(self):
        form = {'age': 1, 'weight': 1, 'height': 1, 'activity': 'test'}
        test = calories(form)
        self.assertFalse(test.is_valid())

    def test_url_get_calories_result(self):
        response = Client().get('/calories-cal/res/')
        self.assertEqual(response.status_code, 302)

    def test_url_post_calories_result_not_valid(self):
        response = Client().post('/calories-cal/res/', {})
        self.assertEqual(response.status_code, 302)

    def test_url_post_calories_result_valid_but_gender_not_female_and_male(self):
        response = Client().post('/calories-cal/res/', {'age' : 1, 'gender' : 'test', 'weight' : 1, 'height' : 1, 'activity' : 'test'})
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'Calories_Calc/calories-cal.html')

    def test_url_post_calories_result_valid_gender_male_activity_sedentary(self):
        response = Client().post('/calories-cal/res/', {'age': 1, 'gender': 'Male', 'weight': 1, 'height': 1, 'activity': 'Sedentary: little or no execersice'})
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'Calories_Calc/calories-cal.html')

    def test_url_post_calories_result_valid_gender_male_activity_light(self):
        response = Client().post('/calories-cal/res/', {'age': 1, 'gender': 'Male', 'weight': 1, 'height': 1, 'activity': 'Light: exercise 1-3 times/week'})
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'Calories_Calc/calories-cal.html')

    def test_url_post_calories_result_valid_gender_male_activity_Moderate(self):
        response = Client().post('/calories-cal/res/', {'age': 1, 'gender': 'Male', 'weight': 1, 'height': 1, 'activity': 'Moderate: exercise 4-5 times/week'})
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'Calories_Calc/calories-cal.html')

    def test_url_post_calories_result_valid_gender_male_activity_Active(self):
        response = Client().post('/calories-cal/res/', {'age': 1, 'gender': 'Male', 'weight': 1, 'height': 1, 'activity': 'Active: daily exercise or intense exercise 3-4 times/week'})
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'Calories_Calc/calories-cal.html')

    def test_url_post_calories_result_valid_gender_male_activity_Very_Active(self):
        response = Client().post('/calories-cal/res/', {'age': 1, 'gender': 'Male', 'weight': 1, 'height': 1, 'activity': 'Very Active: intense exercise 6-7 times/week'})
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'Calories_Calc/calories-cal.html')

    def test_url_post_calories_result_valid_gender_male_activity_Extra_Active(self):
        response = Client().post('/calories-cal/res/', {'age': 1, 'gender': 'Male', 'weight': 1, 'height': 1, 'activity': 'Extra Active: very intense exercise daily, or physical job'})
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'Calories_Calc/calories-cal.html')

    def test_url_post_calories_result_valid_gender_female_activity_sedentary(self):
        response = Client().post('/calories-cal/res/', {'age': 1, 'gender': 'Female', 'weight': 1, 'height': 1, 'activity': 'Sedentary: little or no execersice'})
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'Calories_Calc/calories-cal.html')

    def test_url_post_calories_result_valid_gender_female_activity_light(self):
        response = Client().post('/calories-cal/res/', {'age': 1, 'gender': 'Female', 'weight': 1, 'height': 1, 'activity': 'Light: exercise 1-3 times/week'})
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'Calories_Calc/calories-cal.html')

    def test_url_post_calories_result_valid_gender_female_activity_Moderate(self):
        response = Client().post('/calories-cal/res/', {'age': 1, 'gender': 'Female', 'weight': 1, 'height': 1, 'activity': 'Moderate: exercise 4-5 times/week'})
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'Calories_Calc/calories-cal.html')

    def test_url_post_calories_result_valid_gender_female_activity_Active(self):
        response = Client().post('/calories-cal/res/', {'age': 1, 'gender': 'Female', 'weight': 1, 'height': 1, 'activity': 'Active: daily exercise or intense exercise 3-4 times/week'})
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'Calories_Calc/calories-cal.html')

    def test_url_post_calories_result_valid_gender_female_activity_Very_Active(self):
        response = Client().post('/calories-cal/res/', {'age': 1, 'gender': 'Female', 'weight': 1, 'height': 1, 'activity': 'Very Active: intense exercise 6-7 times/week'})
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'Calories_Calc/calories-cal.html')

    def test_url_post_calories_result_valid_gender_female_activity_Extra_Active(self):
        response = Client().post('/calories-cal/res/', {'age': 1, 'gender': 'Female', 'weight': 1, 'height': 1, 'activity': 'Extra Active: very intense exercise daily, or physical job'})
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'Calories_Calc/calories-cal.html')