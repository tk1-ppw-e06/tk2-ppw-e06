from django.apps import AppConfig


class CaloriesCalcConfig(AppConfig):
    name = 'Calories_Calc'
