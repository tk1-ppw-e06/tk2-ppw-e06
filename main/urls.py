from django.urls import path

from . import views

app_name = 'main'

urlpatterns = [
    path('', views.home, name='home'),
    path('aboutus/', views.aboutus, name='aboutus'),
    path('mail/', views.mail, name='mail'),
    path('searchusername/', views.searchusername, name='serarchusername'),
]
