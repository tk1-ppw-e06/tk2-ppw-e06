from django import forms
from .models import Mail

class mailForm(forms.ModelForm):
    class Meta:
        model = Mail
        fields = ['mail']