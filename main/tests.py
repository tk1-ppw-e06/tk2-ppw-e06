from django.contrib.auth.models import User
from django.test import LiveServerTestCase, TestCase, tag, Client
from django.urls import reverse
from selenium import webdriver


@tag('functional')
class FunctionalTestCase(LiveServerTestCase):
    """Base class for functional test cases with selenium."""

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        # Change to another webdriver if desired (and update CI accordingly).
        options = webdriver.chrome.options.Options()
        # These options are needed for CI with Chromium.
        options.headless = True  # Disable GUI.
        options.add_argument('--no-sandbox')
        options.add_argument('--disable-dev-shm-usage')
        cls.selenium = webdriver.Chrome(options=options)

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super().tearDownClass()


class MainTestCase(TestCase):
    def setUp(self):
        self.account = User.objects.create_user(username='admin', email='admin@example.com', password='admin123')
        self.account.save()
        return super().setUp()

    def test_root_url_status_200(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)
        # You can also use path names instead of explicit paths.
        response = self.client.get(reverse('main:home'))
        self.assertEqual(response.status_code, 200)

    def test_aboutUs_url_status_200(self):
        response = self.client.get('/aboutus/')
        self.assertEqual(response.status_code, 200)

    def test_form_crate_mail_succes(self):
        response = self.client.post('/mail/', data={'mail': 'AIUEO@test.com', 'nextURL' : '/'})
        self.assertEqual(response.status_code, 200)
        isi = response.content.decode('utf8')
        self.assertIn('true', isi)

    def test_form_crate_mail_failed(self):
        response = self.client.post('/mail/', data={'mail': 'AIUEO', 'nextURL' : '/'})
        self.assertEqual(response.status_code, 200)
        isi = response.content.decode('utf8')
        self.assertIn('false', isi)

    def test_url_searchusername_is_exist(self):
        c = Client()
        c.login(username='admin', password='admin123')
        response = c.get('/searchusername/?q=admin')
        self.assertEqual(response.status_code, 200)
        isi = response.content.decode('utf8')
        self.assertIn('admin', isi)
        self.assertIn('true', isi)

    def test_url_searchusername_not_exist(self):
        c = Client()
        response = c.get('/searchusername/?q=test')
        self.assertEqual(response.status_code, 200)
        isi = response.content.decode('utf8')
        self.assertIn('test', isi)
        self.assertIn('false', isi)


class MainFunctionalTestCase(FunctionalTestCase):
    def test_root_url_exists(self):
        self.selenium.get(f'{self.live_server_url}/')
        html = self.selenium.find_element_by_tag_name('html')
        self.assertNotIn('not found', html.text.lower())
        self.assertNotIn('error', html.text.lower())

