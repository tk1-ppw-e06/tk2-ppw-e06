from django.shortcuts import render, redirect
from .forms import mailForm
from django.contrib.auth.models import User
from django.http import JsonResponse

def home(request):
    return render(request, 'main/home.html')

def aboutus(request):
    return render(request,"main/aboutus.html")

def mail(request):
    if request.method == "POST":
        data = {}
        form = mailForm(request.POST)
        if form.is_valid():
            form.save()
            data = {'boolean': True}
            return JsonResponse(data, safe=False)
        else:
            data = {'boolean' : False}
            return JsonResponse(data, safe=False)

def searchusername(request):
    arg = request.GET['q']
    data = {}
    try:
        username = User.objects.get(username=arg)
        data = {'username' : username.username, 'boolean' : 'true'}
    except:
        data = {'username' : arg, 'boolean' : 'false'}
    return JsonResponse(data, safe=False)
