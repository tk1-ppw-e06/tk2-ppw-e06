// focus-blur javascript
$("#age-form").focus( function () {
    $(this).css("background-color", "#FFA274");
});
$("#age-form").blur( function () {
    $(this).css("background-color", "#FFFFFF");
});

$("#gender-form").focus( function () {
    $(this).css("background-color", "#FFA274");
});
$("#gender-form").blur( function () {
    $(this).css("background-color", "#FFFFFF");
});

$("#height-form").focus( function () {
    $(this).css("background-color", "#FFA274");
});
$("#height-form").blur( function () {
    $(this).css("background-color", "#FFFFFF");
});

$("#weight-form").focus( function () {
    $(this).css("background-color", "#FFA274");
});
$("#weight-form").blur( function () {
    $(this).css("background-color", "#FFFFFF");
});

// toggle javascipts
$("#related-article").click(function () {
    $("#article").toggle("fast");
    $("#read-more").toggle("fast");
});

// buat form
$('#Calculate').hide();
$('#header').click(function() {
    $('#Calculate').slideDown(1000);    
});

// buat result login
$('#result').hide();
$('#user-header').click(function() {
    $('#result').slideDown(1000);  
});

$('#result-2').hide();
$('#user-header').click(function() {
    $('#result-2').slideDown(1000);  
});

$('#result-3').hide();
$('#user-header').click(function() {
    $('#result-3').slideDown(1000);  
});

// buat result not-login
$('#result').hide();
$('#not-user-header').click(function() {
    $('#result').slideDown(1000);    
});

// test button
$('#bton1').hide();
$('#header').click(function() {
    $('#bton1').slideDown(1000);    
});

$(document).ready(function () {
    $('.test').click(function () {
        var age = $('.age').val()
        var height = $('.height').val()
        var weight = $('.weight').val()
        var gender = $('#gender-form').val()
        if (age !== '' & height !== '' & weight !== '') {
            $.ajax({
            url: '/bmrcalculator/submit/',
            type: 'GET',
            data: {'age' : age, 'height' : height, 'weight' : weight, 'gender' : gender},
            success: function (data) {
                var result = data.result
                if (result !== 'invalid') {
                    alert("Your result: " + result)
                } else {
                    alert("Error result")
                }
            }
        })
        }
    })
});