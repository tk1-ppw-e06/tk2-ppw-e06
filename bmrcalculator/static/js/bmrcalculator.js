// focus-blur javascript
$("#age-form").focus( function () {
    $(this).css("background-color", "#FFA274");
});
$("#age-form").blur( function () {
    $(this).css("background-color", "#FFFFFF");
});

$("#gender-form").focus( function () {
    $(this).css("background-color", "#FFA274");
});
$("#gender-form").blur( function () {
    $(this).css("background-color", "#FFFFFF");
});

$("#height-form").focus( function () {
    $(this).css("background-color", "#FFA274");
});
$("#height-form").blur( function () {
    $(this).css("background-color", "#FFFFFF");
});

$("#weight-form").focus( function () {
    $(this).css("background-color", "#FFA274");
});
$("#weight-form").blur( function () {
    $(this).css("background-color", "#FFFFFF");
});

// toggle javascipts
// $("#bmr-cal").click(function () {
//     $("#card-click").toggle("fast");
// });

$('#Calculate').hide();
$('#header').click(function() {
    $('#Calculate').slideDown(1000);    
});

$('#bton1').hide();
$('#header').click(function() {
    $('#bton1').slideDown(1000);    
});

$(document).ready(function () {
    $('.test').click(function () {
        var age = $('.age').val()
        var height = $('.height').val()
        var weight = $('.weight').val()
        var gender = $('#gender-form').val()
        if (age !== '' & height !== '' & weight !== '') {
            $.ajax({
            url: '/bmrcalculator/submit/',
            type: 'GET',
            data: {'age' : age, 'height' : height, 'weight' : weight, 'gender' : gender},
            success: function (data) {
                var result = data.result
                if (result !== 'invalid') {
                    alert("Your result: " + result)
                } else {
                    alert("Error result")
                }
            }
        })
        } else {
            alert("Error result")
        }
    })
});



