from django.apps import AppConfig


class BmrcalculatorConfig(AppConfig):
    name = 'bmrcalculator'
