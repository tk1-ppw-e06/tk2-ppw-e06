from django import forms

class bmrcal(forms.Form):
    error_messages = {
        'required': 'This field is required.'
    }

    GENDER_CHOICES= [
    ('male', 'Male'),
    ('female', 'Female'),
    ]
    age = forms.IntegerField(widget=forms.TextInput(attrs={'class': 'form-control age','style': 'border-radius: 10px', 'id': 'age-form'}), label='Age ', required=True)
    gender = forms.CharField(label='Gender ', widget=forms.Select(choices=GENDER_CHOICES, attrs={'style': 'width: 100%; height: 40px; border-radius: 10px;', 'id': 'gender-form'}))
    height = forms.IntegerField(widget=forms.TextInput(attrs={'class': 'form-control height','style': 'border-radius: 10px', 'id': 'height-form'}), label='Height (cm) ', required=True)
    weight = forms.IntegerField(widget=forms.TextInput(attrs={'class': 'form-control weight','style': 'border-radius: 10px', 'id': 'weight-form'}), label='Weight (kg) ', required=True)
    