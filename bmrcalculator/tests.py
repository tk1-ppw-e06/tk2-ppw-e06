from django.test import TestCase, Client
from django.urls import resolve
from django.contrib.auth.models import User

from .models import BMR
from . import views
from Article_Features.models import Articles
from django.core.files.uploadedfile import SimpleUploadedFile

# Create your tests here.

class UnitTestBMRCalculator(TestCase):
    def setUp(self):
        self.account = User.objects.create_user(username='admin', email='admin@example.com', password='admin123',
                                                first_name='test123', last_name='test123')
        self.account.save()
        return super().setUp()

    def test_response_page(self):
        response = Client().get('/bmrcalculator/')
        self.assertEqual(response.status_code, 200)

    def test_title_body(self):
        response = Client().get('/bmrcalculator/')
        title_body_html = response.content.decode('utf8')
        self.assertIn("BMR Calculator", title_body_html)

    def test_calculate_button(self):
        response = Client().get('/bmrcalculator/')
        body_button = response.content.decode('utf8')
        self.assertIn('<button type="submit"  class="calculate btn btn" id="bton">Calculate</button>', body_button)

    def test_bmrcalculator_template(self):
        response = Client().get('/bmrcalculator/')
        self.assertTemplateUsed(response, 'bmrcalculator.html')

    def test_views_forms(self):
        found = resolve('/bmrcalculator/')
        self.assertEqual(found.func, views.form_bmr)

    def test_views_post(self):
        found = resolve('/bmrcalculator/post/')
        self.assertEqual(found.func, views.post_bmrcal)

    def test_models_BMR(self):
        BMR.objects.create(age=1, gender='test', height=1, weight=1)
        count = BMR.objects.all().count()
        self.assertEqual(count, 1)
        test = BMR.objects.get(age=1)

    def test_url_get_view_postBMR(self):
        response = Client().get('/bmrcalculator/post/')
        self.assertEqual(response.status_code, 302)

    def test_url_post_view_postBMR_notvalid(self):
        response = Client().post('/bmrcalculator/post/', {'age' : 1, 'height' : 1, 'weight' : 1})
        self.assertEqual(response.status_code, 302)

    def test_url_post_view_postGMR_genderMale_not_login(self):
        response = Client().post('/bmrcalculator/post/', {'age': 1, 'gender' : 'male', 'height': 1, 'weight': 1})
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'bmrresult.html')

    def test_url_post_view_postGMR_genderMale_has_login(self):
        c = Client()
        c.login(username='admin', password='admin123') #buat login
        small_gif = (
            b'\x47\x49\x46\x38\x39\x61\x01\x00\x01\x00\x00\x00\x00\x21\xf9\x04'
            b'\x01\x0a\x00\x01\x00\x2c\x00\x00\x00\x00\x01\x00\x01\x00\x00\x02'
            b'\x02\x4c\x01\x00\x3b' #utk gambar di article
        )
        image = SimpleUploadedFile('small.gif', small_gif, content_type='image/gif') #definisi file gif
        Articles.objects.create(tittle="Best Fitting Prediction Equations for Basal Metabolic Rate: Informing Obesity Interventions",
                                author=self.account, description="PPW is Fun", image=image)

        response = c.post('/bmrcalculator/post/', {'age': 1, 'gender' : 'male', 'height': 1, 'weight': 1})
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'bmrresult.html')

    def test_url_post_view_postGMR_genderFeMale_not_login(self):
        c = Client()
        response = c.post('/bmrcalculator/post/', {'age': 1, 'gender' : 'female', 'height': 1, 'weight': 1})
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'bmrresult.html')


    def test_url_post_view_postGMR_genderFeMale_has_login(self):
        c = Client()
        c.login(username='admin', password='admin123')
        small_gif = (
            b'\x47\x49\x46\x38\x39\x61\x01\x00\x01\x00\x00\x00\x00\x21\xf9\x04'
            b'\x01\x0a\x00\x01\x00\x2c\x00\x00\x00\x00\x01\x00\x01\x00\x00\x02'
            b'\x02\x4c\x01\x00\x3b'
        )
        image = SimpleUploadedFile('small.gif', small_gif, content_type='image/gif')
        Articles.objects.create(tittle="Best Fitting Prediction Equations for Basal Metabolic Rate: Informing Obesity Interventions",
                                author=self.account, description="PPW is Fun", image=image)

        response = c.post('/bmrcalculator/post/', {'age': 1, 'gender' : 'female', 'height': 1, 'weight': 1})
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'bmrresult.html')

    def test_url_ajax_get_male(self):
        response = Client().get('/bmrcalculator/submit/?age=12&gender=male&height=12&weight=15')
        self.assertEqual(response.status_code, 200)
        isi = response.content.decode('utf8')
        self.assertIn('249.9', isi)

    def test_url_ajax_get_female(self):
        response = Client().get('/bmrcalculator/submit/?age=12&gender=female&height=12&weight=15')
        self.assertEqual(response.status_code, 200)
        isi = response.content.decode('utf8')
        self.assertIn('774.2', isi)

    def test_url_ajax_get_invalid(self):
        response = Client().get('/bmrcalculator/submit/?age=sa&gender=male&height=23&weight=23')
        self.assertEqual(response.status_code, 200)
        isi = response.content.decode('utf8')
        self.assertIn('invalid', isi)