from django.db import models

# Create your models here.
class BMR(models.Model):
    age = models.IntegerField()
    gender = models.CharField(max_length=30)
    height = models.IntegerField()
    weight = models.IntegerField()

    def __str__(self):
        return self.age

