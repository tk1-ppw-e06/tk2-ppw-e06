$(document).ready(function () {
    $(document).on('submit', '#email-submission', function (e){
        e.preventDefault();
        var x = $('#email-submission');
        $.ajax({
            url: '/mail/',
            type: 'POST',
            data: x.serialize(),
            header: {'X-CSRFtoken' : '{% csrf_token %}'},
            success: function (data) {
                var status = data.boolean
                if (status) {
                    alert("Success")
                    document.getElementById('email').value = '';
                } else {
                    alert("Failed")
                }
            }
        })
    })
})